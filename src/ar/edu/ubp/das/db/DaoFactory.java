package ar.edu.ubp.das.db;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DaoFactory {

    private static Context environment;

    private DaoFactory() { }
    
    static {
    	try {
	        DaoFactory.environment = Context.class.cast((new InitialContext()).lookup("java:comp/env"));	
		} 
		catch (NamingException ex) {
		}    	
    }

    @SuppressWarnings({ "unchecked" })
	public static <T,S> Dao<T,S> getDao(String daoName, String daoPackage) throws SQLException {
        try {
        	Class<Dao<T,S>> clazz = (Class<Dao<T,S>>) Class.forName(DaoFactory.getDaoClassName(daoName, daoPackage));
        	return clazz.getDeclaredConstructor().newInstance();
        }
        catch(ClassNotFoundException | IllegalArgumentException | SecurityException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new SQLException(ex.getMessage());
        } 
    }

    private static String getDaoClassName(String daoName, String daoPackage) throws SQLException {
        ResourceBundle rb = ResourceBundle.getBundle("database");
		String daoPrefix = rb.getString("DaoFactoryPrefix");
		return daoPackage + ".daos." + daoPrefix + daoName + "Dao";
    }

}

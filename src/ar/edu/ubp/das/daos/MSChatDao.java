package ar.edu.ubp.das.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.db.Dao;

public class MSChatDao extends Dao<MensajeBean, MensajeBean>{

	@Override
	public MensajeBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		MensajeBean bean = new MensajeBean();
		bean.setFecha(result.getDate("fecha"));
		bean.setMensaje(result.getString("mensaje"));
		bean.setUsuario(result.getString("usuario"));
		bean.setId(result.getInt("id"));
		return bean;
	}

	@Override
	public MensajeBean insert(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setProcedure("agregar_mensaje_usuario(?,?)");
			this.setParameter(1, bean.getAsistenciaId());
			this.setParameter(2, bean.getMensaje());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public MensajeBean update(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MensajeBean delete(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MensajeBean> select(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			if(! bean.getUsuario().contentEquals("")) {
				this.setProcedure("nuevos_mensajes_entidad(?)");	
			} else {
				this.setProcedure("select * from chat_historial where enviado != 1 and id_asistencia = ?");	
			}
			this.setParameter(1, bean.getAsistenciaId());
			return this.executeQuery();
		}
		finally {
			this.close();
		}
	}

	public ArrayList<String> mensajesSinEnviar(int idAsistencia) throws SQLException {
		try {
			this.connect();
			this.setProcedure("nuevos_mensajes_entidad(?)");	
			this.setParameter(1, idAsistencia);
			ResultSet result = this.getStatement().executeQuery();
			ArrayList<String> mensajes = new ArrayList<String>();
			while(result.next()) {
				mensajes.add(result.getString("mensaje"));
			}
			return mensajes;
		}
		finally {
			this.close();
		}
	}
	
	@Override
	public boolean valid(MensajeBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public void agregarMensajesUsuario(int idAsistencia, List<String> mensajes) throws SQLException {
		try {
			this.connect();
			for(String m: mensajes) {
				this.setProcedure("agregar_mensaje_usuario(?,?)");
				this.setParameter(1, idAsistencia);
				this.setParameter(2, m);
				this.executeUpdate();	
			}
		}
		finally {
			this.close();
		}
	}
}

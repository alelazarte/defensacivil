package ar.edu.ubp.das.daos;

import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.db.Dao;

public class MSSesionesDao extends Dao<UsuarioBean, UsuarioBean> {
	
	private String generarToken() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		return bytes.toString();
	}
	
	@Override
	public UsuarioBean make(ResultSet result) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioBean insert(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select clave from usuarios where usuario = ?");
			this.setParameter(1, bean.getUsuario());
			ResultSet result = this.getStatement().executeQuery();
			if(! result.next()) {
				throw new SQLException("Usuario no existente");
			}
			if (! result.getString("clave").contentEquals(bean.getClave())) {
				throw new SQLException("Clave incorrecta");
			}
			bean.setToken(this.generarToken());
			this.setProcedure("iniciar_sesion(?,?)");
			this.setParameter(1, bean.getUsuario());
			this.setParameter(2, bean.getToken());
			this.executeUpdate();
			return bean;
		}
		finally {
			this.close();
		}
	}

	@Override
	public UsuarioBean update(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuarioBean delete(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UsuarioBean> select(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean valid(UsuarioBean bean) throws SQLException {
		// TODO Auto-generated method stub
		try {
			this.connect();
			this.setStatement("select token from sesiones where id_usuario = (select id from usuarios where usuario = ?)");
			this.setParameter(1, bean.getUsuario());
			ResultSet result = this.getStatement().executeQuery();
			if(! result.next()) {
				throw new SQLException("Sesion no encontrada");
			}
			// TODO fecha valida de token
			if(result.getString("token").contentEquals(bean.getToken())) {
				return true;
			}
			return false;
		}
		finally {
			this.close();
		}
	}

}

package ar.edu.ubp.das.ws;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ar.edu.ubp.das.beans.AsistenciaBean;
import ar.edu.ubp.das.beans.MensajeBean;
import ar.edu.ubp.das.beans.UsuarioBean;
import ar.edu.ubp.das.daos.MSChatDao;
import ar.edu.ubp.das.db.Dao;
import ar.edu.ubp.das.db.DaoFactory;

public class DefensaCivilWS {
	
	private boolean tokenValido(String usuario, String token) throws SQLException {
		Dao<Void, UsuarioBean> dao = DaoFactory.getDao("Sesiones", "ar.edu.ubp.das");
		UsuarioBean bean = new UsuarioBean();
		bean.setUsuario(usuario);
		bean.setToken(token);
		return dao.valid(bean);
	}
	
	public int solicitarAsistencia(String usuario, String token, String cuil, String geolocalizacion, String mensaje, String urlAdjuntos) throws SQLException {
		// TODO
		if(! this.tokenValido(usuario, token)) {
			return -1;
		}
		Dao<AsistenciaBean, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
		AsistenciaBean bean = new AsistenciaBean();
		bean.setCuil(cuil);
		bean.setGeolocalizacion(geolocalizacion);
		bean.setMensaje(mensaje);
		bean.setUrlAdjuntos(urlAdjuntos);
		AsistenciaBean asistencia = dao.insert(bean);
		return asistencia.getId();
	}
	
	public void cancelarAsistencia(String usuario, String token, int id) throws SQLException {
		// TODO
		if(! this.tokenValido(usuario, token)) {
			return;
		}
		Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
		AsistenciaBean bean = new AsistenciaBean();
		bean.setId(id);
		dao.delete(bean);
	}
	
	public boolean isAsistenciaFinalizada(String usuario, String token, int id) throws SQLException {
		if(! this.tokenValido(usuario, token)) {
			return false;
		}
		Dao<Void, AsistenciaBean> dao = DaoFactory.getDao("Asistencias", "ar.edu.ubp.das");
		AsistenciaBean bean = new AsistenciaBean();
		bean.setId(id);
		return dao.valid(bean);
	}
	
	public String obtenerToken(String usuario, String clave) throws SQLException {
		// TODO
		UsuarioBean bean = new UsuarioBean();
		bean.setUsuario(usuario);
		bean.setClave(clave);
		Dao<UsuarioBean, UsuarioBean> dao = DaoFactory.getDao("Sesiones", "ar.edu.ubp.das");
		return dao.insert(bean).getToken();
	}
	
	public void agregarMensajeUsuario(String usuario, String token, int asistenciaId, String mensaje) throws SQLException {
		// TODO
		if(! this.tokenValido(usuario, token)) {
			return;
		}
		Dao<Void, MensajeBean> dao = DaoFactory.getDao("Chat", "ar.edu.ubp.das");
		MensajeBean bean = new MensajeBean();
		bean.setAsistenciaId(asistenciaId);
		bean.setMensaje(mensaje);
		dao.insert(bean);
	}
	
	public void agregarMensajesUsuario(String usuario, String token, int asistenciaId, List<String> mensajes) throws SQLException {
		MSChatDao dao = new MSChatDao();
		dao.agregarMensajesUsuario(asistenciaId, mensajes);
		dao.close();
	}
	
	public List<String> nuevosMensajesEntidad(String usuario, String token, int idAsistencia) throws SQLException {
		// TODO
		if(! this.tokenValido(usuario, token)) {
			return null;
		}
		
		MSChatDao dao = new MSChatDao();
		ArrayList<String> mensajes = dao.mensajesSinEnviar(idAsistencia);
		dao.close();
		
		return mensajes;
	}
	
	public static void main(String[] args) throws SQLException {
		
		DefensaCivilWS servicio = new DefensaCivilWS();
		
		String token = servicio.obtenerToken("admin", "admin");
		int id = servicio.solicitarAsistencia("admin", token, "cuil", "geolo", "memememe", "url1, url2");
		
		boolean itis = servicio.isAsistenciaFinalizada("admin", token, id);
		System.out.println("finalizada: "+itis);
		
		MensajeBean mensaje = new MensajeBean();
		mensaje.setAsistenciaId(1);
		mensaje.setMensaje("un nuevo mensaje de un usuario");
		mensaje.setUsuario("usuario");
		servicio.agregarMensajeUsuario("admin", token, id, "asd asd");
		
		List<String> mensajes = servicio.nuevosMensajesEntidad("admin", token, id);

		if(mensajes == null) {
			System.out.println("no hay mensajes");
		} else {
			mensajes.forEach(m -> {
				System.out.println(m);
			});
			System.out.println("fin mensajes");
		}
		
		servicio.cancelarAsistencia("admin", token, id);
		
			/*
			AsistenciaBean asistencia = new AsistenciaBean();
			asistencia.setCuil("cuil");
			asistencia.setGeolocalizacion("asdasdasd");
			asistencia.setMensaje("mensajje");
			servicio.solicitarAsistencia("token", asistencia);
			
			MensajeBean mensaje = new MensajeBean();
			mensaje.setAsistenciaId(1);
			mensaje.setMensaje("me fijo, gracias");
			servicio.agregarMensajeUsuario(mensaje);
			
			List<MensajeBean> historial = servicio.nuevosMensajesEntidad("admin", "123", 1, 9);
			if(historial == null) {
				System.out.println("no mensajes nuevos");
			} else {
				historial.forEach(m -> {
					System.out.println(m.getMensaje());
				});
				System.out.println("ultimo id de mensaje es " + historial.get(historial.size()-1).getId());
			}
			/*
			System.out.println(servicio.isAsistenciaFinalizada(1));
			servicio.cancelarAsistencia("token", 1);
			System.out.println(servicio.isAsistenciaFinalizada(1));
			
			/*
			String token = servicio.obtenerToken("admin", "admin");
			System.out.println(token);
			System.out.println(servicio.tokenValido("admin", token));
			System.out.println(servicio.tokenValido("asd", token));
			*/
	}
}

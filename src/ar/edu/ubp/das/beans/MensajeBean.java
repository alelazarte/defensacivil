package ar.edu.ubp.das.beans;

import java.sql.Date;

public class MensajeBean {
	int id, idAsistencia;
	String usuario, mensaje;
	Date fecha;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAsistenciaId() {
		return idAsistencia;
	}
	public void setAsistenciaId(int asistenciaId) {
		this.idAsistencia = asistenciaId;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
